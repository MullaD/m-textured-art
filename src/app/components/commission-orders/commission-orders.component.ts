import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-commission-orders',
  standalone: true,
  imports: [ButtonModule,RouterLink],
  templateUrl: './commission-orders.component.html',
})
export class CommissionOrdersComponent {

}
