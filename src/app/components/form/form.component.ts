import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule,Validators,FormBuilder } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';

@Component({
  selector: 'app-form',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './form.component.html',
})
export class FormComponent {
  submitted: boolean = false;
  form = this.fb.nonNullable.group({
    name: [''],
    email: ['', Validators.email],
    phone: [''],
    comment: [''],
  });
  constructor(private fb: FormBuilder) {}

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    } else {
      this.form.patchValue({
        name: this.form.value.name,
        email: this.form.value.email,
        phone: this.form.value.phone,
        comment: this.form.value.comment,
      });
    }
    this.form.reset();
  }
}
