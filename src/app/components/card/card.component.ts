import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardItem } from '../cards-section/cards-section.component';


@Component({
  selector: 'app-card',
  standalone: true,
  imports: [ButtonModule, CommonModule, RouterLink],
  templateUrl: './card.component.html',
})
export class CardComponent {

@Input() item!:CardItem


}
