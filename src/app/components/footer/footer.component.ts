import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [],
  template: `
    <div class="mt-8 bg ">
      <footer>
        <p class="py-5 text-sm text-center text-color-secondary">
          M.Textured Art © 2023
        </p>
      </footer>
    </div>
  `,
  styles: `

  .bg{
    background-color:#f1efeb;
  }
  `,
})
export class FooterComponent {}
