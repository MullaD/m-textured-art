import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { ButtonModule } from 'primeng/button';

export interface CardItem {
  image?: string;
  bgImage?: string;
  title?: string;
  price?: string;
  description?:string;
  buttonLabel?:string;
  navLink?:string;
  class?:string;
  social?:string;
}
@Component({
  selector: 'app-cards-section',
  standalone: true,
  imports: [CommonModule, CardComponent,ButtonModule],
  templateUrl: './cards-section.component.html',
})
export class CardsSectionComponent {
  items: CardItem[] = [
    {
      bgImage: '../../../assets/images/clouds.webp',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/1.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/2.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/3.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/4.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/5.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/8.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
    {
      bgImage: '../../../assets/images/black1.jpg',
      title: 'Cold Brew',
      price: '$220.00 AUD',
    },
  ];

  @Input() title!:string;

  onOrder(event:any){
    event.stopPropagation();
    window.open('https://web.whatsapp.com/')
  }
}
