import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { CardComponent } from '../../components/card/card.component';

@Component({
  selector: 'app-socials',
  standalone: true,

  imports: [CommonModule, CardComponent],
  templateUrl: './socials.component.html',
})
export class SocialsComponent {
  socials = [
    {
      image: '../../../assets/images/logo.png',
      social: 'COMMISIONS',
      description:
        'Commission orders welcome.No exchanges or refunds available on custom made to order items.',
      buttonLabel: 'ENQUIRE NOW',
      navLink: 'contact',
    },
    {
      image: '../../../assets/images/instagram.png',
      social: 'INSTAGRAM',
      description:
        'Follow @m_textured_art to view behind the scenes process reels, customer pics, updates on new designs and more.',
      buttonLabel: 'FOLLOW',
      navLink: 'https://www.instagram.com/m_textured_art/?igsh=b3Fuczl2N2x0MXJl',
    },
    {
      image: '../../../assets/images/delivery.png',
      social: 'DELIVERY',
      description: 'Albania delivery available',
    },
  ];
}
