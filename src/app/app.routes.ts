import { Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ShopPageComponent } from './pages/shop-page/shop-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';

export const routes: Routes = [
  {path:'',component: HomePageComponent},
  {path:'shop',component: ShopPageComponent},
  {path:'contact', component:ContactPageComponent },
  {path:'about', component: AboutPageComponent },
  {path:'details', component: DetailsPageComponent }
];
