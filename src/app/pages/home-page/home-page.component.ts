import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardsSectionComponent } from '../../components/cards-section/cards-section.component';
import { CommissionOrdersComponent } from '../../components/commission-orders/commission-orders.component';
import { SocialsComponent } from '../../components/socials/socials.component';

@Component({
  selector: 'app-home-page',
  standalone: true,
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss',
  imports: [
    CommonModule,
    ButtonModule,
    CardsSectionComponent,
    CommissionOrdersComponent,
    RouterLink,
    SocialsComponent,
  ],
})
export class HomePageComponent {
  title = 'FEATURED PRODUCTS';
}
