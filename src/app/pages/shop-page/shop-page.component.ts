import { Component, Input } from '@angular/core';
import { CardsSectionComponent } from '../../components/cards-section/cards-section.component';
import { CommissionOrdersComponent } from '../../components/commission-orders/commission-orders.component';

@Component({
  selector: 'app-shop-page',
  standalone: true,
  templateUrl: './shop-page.component.html',
  imports: [CardsSectionComponent, CommissionOrdersComponent],
})
export class ShopPageComponent {
  title = 'All Products';
}
