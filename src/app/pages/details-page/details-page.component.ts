import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CarouselComponent } from '../../components/carousel/carousel.component';
import { CommissionOrdersComponent } from '../../components/commission-orders/commission-orders.component';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-details-page',
  standalone: true,
  templateUrl: './details-page.component.html',
  imports: [
    RouterLink,
    ButtonModule,
    CommonModule,
    CommissionOrdersComponent,
    CarouselComponent,
  ],
})
export class DetailsPageComponent {
  art = [
    '../../../assets/images/7.jpg',
    '../../../assets/images/2.jpg',
    '../../../assets/images/1.jpg',
    '../../../assets/images/3.jpg',
  ];
  images = [
    '../../../assets/images/7.jpg',
    '../../../assets/images/2.jpg',
    '../../../assets/images/1.jpg',
    '../../../assets/images/4.jpg',
    '../../../assets/images/3.jpg',
    '../../../assets/images/5.jpg',
  ];


  onOrder(event:any){
    window.open('https://web.whatsapp.com/')
  }
}
