import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormComponent } from "../../components/form/form.component";
@Component({
    selector: 'app-contact-page',
    standalone: true,
    templateUrl: './contact-page.component.html',
    imports: [CommonModule, FormComponent]
})
export class ContactPageComponent {}
